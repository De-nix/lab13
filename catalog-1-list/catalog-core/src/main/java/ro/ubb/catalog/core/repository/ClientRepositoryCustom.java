package ro.ubb.catalog.core.repository;


import ro.ubb.catalog.core.model.Client;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by radu.
 */
public interface ClientRepositoryCustom {
    List<Client> findAllClientsWithBookNameJPQL(String title);
    List<Client> findAllClientsWithBookNameCriteriaAPI(String title);
    List<Client> findAllClientsWithBookNameSQL(String title);
    List<Client> findAllClientsWithPurchaseDateJPQL(Timestamp date);
    List<Client> findAllClientsWithPurchaseDateCriteriaAPI(Timestamp date);
    List<Client> findAllClientsWithPurchaseDateSQL(Timestamp date);
}
