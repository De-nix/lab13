package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */


@NamedEntityGraphs({
        @NamedEntityGraph(name = "bookWithPurchases",
                attributeNodes = @NamedAttributeNode(value = "purchases")),
        @NamedEntityGraph(name = "bookWithPurchasesAndClients",
                attributeNodes = @NamedAttributeNode(value = "purchases", subgraph = "purchasesWithClients"),
                subgraphs = @NamedSubgraph(name = "purchasesWithClients",
                        attributeNodes = @NamedAttributeNode(value = "client")))
})
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder

@EqualsAndHashCode(exclude = {"purchases"}, callSuper = true)
@ToString(exclude = {"purchases"})
public class Book extends BaseEntity<Long> {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "price", nullable = false)
    private Integer price;


    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Purchase> purchases;

    public Set<Client> getClients() {
        return purchases.stream()
                .map(Purchase::getClient).collect(Collectors.toUnmodifiableSet());
    }

    public void addClient(Client client) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setBook(this);
        purchases.add(purchase);
    }

    public void addPurchase(Client client, Timestamp date) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setDate(date);
        purchase.setBook(this);
        purchases.add(purchase);
    }
    public void deletePurchase(Long clientId){
        purchases.removeIf(x-> x.getClient().getId().equals(clientId));

    }

}
