package ro.ubb.catalog.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Client;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends StoreRepository<Client, Long>, ClientRepositoryCustom{

    Page<Client> findAllByNameContaining(String name, Pageable pageable);

    @Query("select distinct a from Client a")
    @EntityGraph(value = "clientWithPurchases", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithPurchases();

    @Query("select distinct a from Client a")
    @EntityGraph(value = "clientWithPurchasesAndBooks", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithPurchasesAndBooks();


    @EntityGraph(value = "clientWithPurchasesAndBooks", type = EntityGraph.EntityGraphType.LOAD)
    Optional<Client> findById(Long id);


}
