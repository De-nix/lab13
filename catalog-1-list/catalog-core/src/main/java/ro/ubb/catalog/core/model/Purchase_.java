package ro.ubb.catalog.core.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Purchase.class)
public abstract class Purchase_  {

	public static volatile SingularAttribute<Purchase, Book> book;
	public static volatile SingularAttribute<Purchase, Client> client;
	public static volatile SingularAttribute<Purchase, Timestamp> date;

}

