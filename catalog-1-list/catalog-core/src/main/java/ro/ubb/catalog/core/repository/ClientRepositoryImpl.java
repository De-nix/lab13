package ro.ubb.catalog.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.*;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * Created by radu.
 */
public class ClientRepositoryImpl extends CustomRepositorySupport implements ClientRepositoryCustom {

    public List<Client> findAllClientsWithBookNameJPQL(String title) {
        System.out.println("jpql");
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct a from Client a " +
                "left join fetch a.purchases b " +
                "left join fetch b.book " +
                "where b.book.title = :title");
        return query.setParameter("title", title).getResultList();
    }



    public List<Client> findAllClientsWithBookNameCriteriaAPI(String title1) {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
        query.distinct(Boolean.TRUE);
        Root<Client> root = query.from(Client.class);
        SetJoin<Client, Purchase> x = root.join(Client_.purchases);
        Join<Purchase, Book> purchase = x.join(Purchase_.book);

        Fetch<Client, Purchase> clientPurchaseFetch = root.fetch(Client_.purchases);
        Fetch<Purchase,Book> books = clientPurchaseFetch.fetch(Purchase_.book);
//        ParameterExpression<String> titleParam = criteriaBuilder.parameter(String.class);
        query.where(criteriaBuilder.like(purchase.get(Book_.title),title1));

        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Transactional
    public List<Client> findAllClientsWithBookNameSQL(String title) {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {a.*},{b.*},{p.*} " +
                "from client a " +
                "left join purchase p on a.id=p.client_id " +
                "left join book b on p.book_id=b.id " +
                "where b.title = '"+title+"'")
                .addEntity("a",Client.class)
                .addJoin("p", "a.purchases")
                .addJoin("b", "p.book")
                .addEntity("a",Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);


        return (List<Client>) query.getResultList();
    }

    @Override
    public List<Client> findAllClientsWithPurchaseDateJPQL(Timestamp date) {

        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct a from Client a " +
                        "left join fetch a.purchases b " +
                        "where b.date < :date");
        return query.setParameter("date", date).getResultList();

    }
    public List<Client> findAllClientsWithPurchaseDateCriteriaAPI(Timestamp date) {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
        query.distinct(Boolean.TRUE);
        Root<Client> root = query.from(Client.class);
        SetJoin<Client, Purchase> x = root.join(Client_.purchases);

        Fetch<Client, Purchase> clientPurchaseFetch = root.fetch(Client_.purchases);
        query.where(criteriaBuilder.lessThan(x.get(Purchase_.date),date));

        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Transactional
    public List<Client> findAllClientsWithPurchaseDateSQL(Timestamp date) {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        return session.createSQLQuery("select distinct {a.*}, {p.*} " +
                "from client a " +
                "left join purchase p on a.id=p.client_id " +
                "where p.date <  '"+date+"'")
                .addEntity("a",Client.class)
                .addJoin("p", "a.purchases")
                .addEntity("a",Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).getResultList();
    }

}
