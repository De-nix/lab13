package ro.ubb.catalog.core.model.Validators;


import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Purchase;

public class PurchaseValidator  {

    public static void validate(Purchase entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no purchase. Sorry.\n");
        if (entity.getDate() == null) throw new ValidatorException("There is no date\n");
        if (entity.getBook() == null) throw new ValidatorException("There is no book\n");
        if (entity.getClient() == null) throw new ValidatorException("There is no client\n");
    }}

