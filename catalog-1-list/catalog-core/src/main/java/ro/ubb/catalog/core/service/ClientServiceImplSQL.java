package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Exceptions.RepositoryException;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.model.Validators.ClientValidator;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.ClientRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Qualifier("SQL")
public class ClientServiceImplSQL implements ClientService {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private BookRepository bookRepository;

    public static final Logger log = LoggerFactory.getLogger(ClientServiceImplSQL.class);

    public  Client addClient(String name) throws ValidatorException {
        Client newClient = Client.builder().name(name).build();
        ClientValidator.validate(newClient);
        return clientRepository.save(newClient);
    }

    public  void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }


    public  Client getClient(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Transactional
    public Client updateClient(Long id, String name, Set<Long> books) throws Exception {

        log.trace("updateClient - method entered: id ={}, client={} books{}", id, name,books);
        clientRepository.findById(id).ifPresent(s -> {
            s.setName(name);
            log.trace("initial nr of bought books {}",s.getPurchases().size());
            s.getPurchases().stream().filter(x -> !books.contains(x.getBook().getId())).collect(Collectors.toList()).forEach(
            x->{
                log.trace("in delete - the deleted entity is {}, with fields {},{},{}",x,x.getBook(),x.getClient(),x.getDate());
                s.deletePurchase(x.getBook().getId());
                x.getBook().deletePurchase(s.getId());

                log.trace("in delete - the deleted entity after the first attempt of deleting entity from client is {}, with fields {},{},{}",x,x.getBook(),x.getClient(),x.getDate());

                    log.trace("after delete -- nr of bought books {}",s.getPurchases().size());
            });
            log.trace("exiting the delete part of update -> moving to the insert one which works");

            Set<Long> ownedBooks = s.getPurchases().stream().map(y -> y.getBook().getId()).collect(Collectors.toSet());
            books.stream().filter(x -> !ownedBooks.contains(x)).forEach(x -> {
                Book book = bookRepository.findById(x)
                        .orElseThrow(() -> new RepositoryException("The id's of the books are not valid ones"));
                s.addPurchase(book, new Timestamp(System.currentTimeMillis()));
            });
            log.debug("updateClient - updated: s={}", s);

        });


        log.trace("updateClient - method finished");
        return clientRepository.findById(id).orElseThrow(()->new Exception("the client was not found"));
    }

    public  List<Client> getAllClients() {
        return clientRepository.findAllWithPurchasesAndBooks();
    }

    public  List<Client> getAllClientsWithBook(String title) {
        System.out.println("in get clients sql");
        return  clientRepository.findAllClientsWithBookNameSQL(title);
    }

    public  List<Client> getAllClientsWithPurchase(Timestamp date) {
        return clientRepository.findAllClientsWithPurchaseDateSQL(date);
    }


    @Transactional
    public void updatePurchaseDate(Long clientId, Long bookId, Timestamp date){
        log.trace("intrare update dates din service Server client{}, book{}, date{}",clientId,bookId,date);

        Optional<Client> result = clientRepository.findById(clientId);
        Purchase p = result.get().getPurchases().stream().filter(y-> y.getBook().getId().equals(bookId)).findFirst().get();
        p.setDate(date);

        log.trace("client after purchase update is {}",result.get());


        log.trace("client after purchase update is {}",result.get());

        log.trace("iesire update dates din service Server");

}

}
