package ro.ubb.catalog.core.repository;


import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by radu.
 */
public interface BookRepositoryCustom {
    List<Book> findAllWithClientNameJPQL(String name);
    List<Book> findAllWithClientNameCriteriaAPI(String name);
    List<Book> findAllWithClientNameSQL(String name);
    List<Book> findAllWithPurchaseDateJPQL(Timestamp date);
    List<Book> findAllWithPurchaseDateCriteriaAPI(Timestamp date);
    List<Book> findAllWithPurchaseDateSQL(Timestamp date);
}
