package ro.ubb.catalog.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * Created by radu.
 */
public class BookRepositoryImpl extends CustomRepositorySupport implements BookRepositoryCustom {

    public List<Book> findAllWithClientNameJPQL(String name) {

        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct a from Book a " +
                        "left join fetch a.purchases b " +
                        "left join fetch b.client " +
                        "where b.client.name = :name");
        return query.setParameter("name", name).getResultList();


    }
    @Override
    public List<Book> findAllWithClientNameCriteriaAPI(String name) {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
        query.distinct(Boolean.TRUE);
        Root<Book> root = query.from(Book.class);
        SetJoin<Book, Purchase> x = root.join(Book_.purchases);
        Join<Purchase, Client> clients = x.join(Purchase_.client);

        Fetch<Book, Purchase> bookPurchaseFetch = root.fetch(Book_.purchases);
        bookPurchaseFetch.fetch(Purchase_.client);
        query.where(criteriaBuilder.like(clients.get(Client_.name),name));

        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    @Transactional
    public List<Book> findAllWithClientNameSQL(String name) {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {a.*},{b.*},{p.*} " +
                "from book a " +
                "left join purchase p on a.id=p.book_id " +
                "left join book b on p.client_id=b.id " +
                "where b.name = '"+name+"'")
                .addEntity("a",Book.class)
                .addJoin("p", "a.purchases")
                .addJoin("b", "p.client")
                .addEntity("a",Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);


        return (List<Book>) query.getResultList();
    }

    @Override
    public List<Book> findAllWithPurchaseDateJPQL(Timestamp date) {

        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct a from Book a " +
                        "left join fetch a.purchases b " +
                        "where b.date < :date");
        return query.setParameter("date", date).getResultList();

    }

    @Override
    public List<Book> findAllWithPurchaseDateCriteriaAPI(Timestamp date) {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
        query.distinct(Boolean.TRUE);
        Root<Book> root = query.from(Book.class);
        SetJoin<Book, Purchase> x = root.join(Book_.purchases);
        root.fetch(Book_.purchases);
        query.where(criteriaBuilder.lessThan(x.get(Purchase_.date),date));

        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Override

    @Transactional
    public List<Book> findAllWithPurchaseDateSQL(Timestamp date) {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        return session.createSQLQuery("select distinct {a.*}, {p.*} " +
                "from book a " +
                "left join purchase p on a.id=p.book_id " +
                "where p.date <  '"+date+"'")
                .addEntity("a",Book.class)
                .addJoin("p", "a.purchases")
                .addEntity("a",Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).getResultList();
    }

}
