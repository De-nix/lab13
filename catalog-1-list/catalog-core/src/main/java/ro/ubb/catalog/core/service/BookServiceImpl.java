package ro.ubb.catalog.core.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Validators.BookValidator;
import ro.ubb.catalog.core.repository.BookRepository;

import java.util.*;

@Service
public class BookServiceImpl implements BookService{
    static int pageCapacity =5;

    @Autowired
    private BookRepository bookRepository;



    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);
    public  Book addBook(String title,String author, int price) throws ValidatorException {
        Book newBook = Book.builder().title(title).author(author).price(price).purchases(new HashSet<>()).build();
        BookValidator.validate(newBook);
        return bookRepository.save(newBook);
    }

    public  void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }
    @Transactional
    public  Book updateBook(Long id, String title,String author, int price) throws ValidatorException {
        log.trace("updateBook - method entered:id={} title={} author={} price={}", id,title,author,price);
        Optional<Book> optionalBook =  bookRepository.findById(id);
        optionalBook.ifPresent(s -> {
            s.setTitle(title);
            s.setAuthor(author);
            s.setPrice(price);
            log.debug("updateBook - updated: s={}", s);
        });
        log.trace("updateBook - method finished");
        return optionalBook.orElse(null);


    }
    public  Book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }


    public List<Book> getAllBooks(){
        return bookRepository.findAllWithPurchasesAndClients();
    }

}
