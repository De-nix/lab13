package ro.ubb.catalog.core.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface ClientService {

     Client addClient(String name) throws ValidatorException;

    /**
     * @param id - the client that will be deleted
     *
     */
     void deleteClient(Long id)  ;


     Client updateClient(Long id, String name, Set<Long> books) throws Exception;

    /**
     * @param id - the id of a client
     * @return - the client with the id = 'id'
     */
     Client getClient(Long id) ;

    /**
     * @return - a set of all the clients
     */

    void updatePurchaseDate(Long clientId, Long bookId, Timestamp date);


    List<Client> getAllClients() ;

    List<Client> getAllClientsWithBook(String title) ;
    List<Client> getAllClientsWithPurchase(Timestamp date);
}
