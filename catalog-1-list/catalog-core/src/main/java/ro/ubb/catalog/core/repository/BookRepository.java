package ro.ubb.catalog.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.util.List;
import java.util.Optional;

/**
 * Created by radu.
 */

@Repository
public interface BookRepository extends StoreRepository<Book, Long>, BookRepositoryCustom{
    Page<Book> findAllByTitleContaining(String title, Pageable pageable);
    Page<Book> findAllByAuthorContaining(String author, Pageable pageable);
    Page<Book> findAllByPriceLessThan(Integer price, Pageable pageable);
    /*query the entities using: Spring Queries with Named Entity Graphs,  JPQL, Criteria API, Native SQL
- in each repository (e.g: BookRepository and ClientRepository) there should be at least two methods using NamedEntityGraphs
- for each repository (e.g: BookRepository and ClientRepository), in the corresponding fragment/customized interface there should be at least two additional methods; these  additional methods should have three different implementations with: JPQL, CriteriaAPI, NativeSql
- in the services only the 'main' repositories should be used (e.g: BookRepository and ClientRepository, not the fragment/customized ones)
- the application should work alternatively with all of the following configurations: EntityGraphs + JPQL, EntityGraphs + CriteriaAPI, EntityGraphs + NativeSql. The configuration switch should be possible by changing annotations or property files, but not java code.

*/
    @Query("select distinct a from Book a")
    @EntityGraph(value = "bookWithPurchases", type = EntityGraph.EntityGraphType.LOAD)
    List<Book> findAllWithPurchases();

    @Query("select distinct a from Book a")
    @EntityGraph(value = "bookWithPurchasesAndClients", type = EntityGraph.EntityGraphType.LOAD)
    List<Book> findAllWithPurchasesAndClients();

    @EntityGraph(value = "bookWithPurchasesAndClients", type = EntityGraph.EntityGraphType.LOAD)
    Optional<Book> findById(Long id);

}
