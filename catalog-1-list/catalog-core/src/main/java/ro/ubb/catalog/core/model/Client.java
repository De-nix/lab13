package ro.ubb.catalog.core.model;

import lombok.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@NamedEntityGraphs({
        @NamedEntityGraph(name = "clientWithPurchases",
                attributeNodes = @NamedAttributeNode(value = "purchases")),
        @NamedEntityGraph(name = "clientWithPurchasesAndBooks",
                attributeNodes = @NamedAttributeNode(value = "purchases", subgraph = "purchasesWithBooks"),
                subgraphs = @NamedSubgraph(name = "purchasesWithBooks",
                        attributeNodes = @NamedAttributeNode(value = "book")))
})
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"purchases"})
public class Client extends BaseEntity<Long> {


    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "client", orphanRemoval = true
            ,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Purchase> purchases;


    public Set<Book> getBooks() {
        purchases = purchases == null ? new HashSet<>() :
                purchases;
        return this.purchases.stream().
                map(Purchase::getBook).collect(Collectors.toUnmodifiableSet());
    }

    @Transactional
    public void addBook(Book book) {
        Purchase purchase = new Purchase();
        purchase.setBook(book);
        purchase.setClient(this);
        purchases.add(purchase);
    }
   @Transactional
    public void deletePurchase(Long bookId){
        purchases.removeIf(x-> x.getBook().getId().equals(bookId));

    }

    @Transactional
    public void setPurchase(Purchase p, Timestamp date){


    }


    public void addBook(Set<Book> books) {
        books.forEach(this::addBook);
    }

    public void addPurchase(Book book, Timestamp date) {
        Purchase purchase = new Purchase();
        purchase.setBook(book);
        purchase.setDate(date);
        purchase.setClient(this);
        purchases.add(purchase);
    }


}
