package ro.ubb.catalog.core.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;

import java.util.List;
import java.util.Set;

public interface BookService {


    Book addBook(String title, String author, int price) throws ValidatorException;

    /**
     * @param id - the id of the book that will be deleted
     * @return
     */
     void deleteBook(Long id) ;


     Book updateBook(Long id, String title, String author, int price) throws ValidatorException ;

    /**
     * @param id - id of the book
     * @return - the book with id = 'id'
     */
    Book getBook(Long id);

    List<Book> getAllBooks();


}
