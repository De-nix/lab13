package ro.ubb.catalog.core.model;


import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ extends BaseEntity_ {

    public static volatile SetAttribute<Book, Purchase> purchases;
	public static volatile SingularAttribute<Book, String> title;
	public static volatile SingularAttribute<Book, String> author;
	public static volatile SingularAttribute<Book, Integer> price;

}

