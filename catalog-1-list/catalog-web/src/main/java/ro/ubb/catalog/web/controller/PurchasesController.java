package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.BookService;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.dto.PurchaseDto;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */

@RestController
public class PurchasesController {
    private static final Logger log = LoggerFactory.getLogger(PurchasesController.class);

    @Autowired

    @Qualifier("API")
    private ClientService clientService;
    @Autowired
    private BookService bookService;


    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.GET)
    public Set<PurchaseDto> getClientBooks(
            @PathVariable final Long id) {
        log.trace("getPurchases: studentId={}", id);

        Set<PurchaseDto> result;
        result= clientService.getClient(id).getPurchases().stream().map(x->{
            LocalDateTime localTime = x.getDate().toLocalDateTime();
            return PurchaseDto.builder().idBook(x.getBook().getId()).idClient(x.getClient().getId())
                    .bookName(x.getBook().getTitle()).date(localTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).build();
        }).collect(Collectors.toSet());

        log.trace("getPurchases: method finished");
        return result;
    }

    @RequestMapping(value = "/purchases/book/{id}", method = RequestMethod.GET)
    public Set<PurchaseDto> getBookClients(
            @PathVariable final Long id) {
        log.trace("getPurchases: studentId={}", id);

        Set<PurchaseDto> result;
        result= bookService.getBook(id).getPurchases().stream().map(x->{
            LocalDateTime localTime = x.getDate().toLocalDateTime();
            return PurchaseDto.builder().idBook(x.getBook().getId()).idClient(x.getClient().getId())
                    .bookName(x.getBook().getTitle()).date(localTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).build();
        }).collect(Collectors.toSet());

        log.trace("getPurchases: method finished");
        return result;
    }

    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.PUT)
    public void updatePurchaseDate(
            @PathVariable final Long id,
            @RequestBody final PurchaseDto purchase) {
        log.trace("updateStudentGrades: studentId={}, studentDisciplineDtos={}",
                id, purchase);
                clientService.updatePurchaseDate(id, purchase.getIdBook(), Timestamp.valueOf(purchase.getDate()+" 00:00:00"));
        log.trace("updateStudentGrades: finished");
    }

}
