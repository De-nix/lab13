package ro.ubb.catalog.web.dto;

import org.springframework.data.domain.Sort;

import java.util.List;

public class SortDto {
    public List<String> fields;
    public List<Boolean> directions;

    public static Sort.Direction transformDirection(Boolean bool){
        if(bool) return Sort.Direction.ASC;
        return Sort.Direction.DESC;
    }

    public Sort getSort(){

        Sort newSort = Sort.by(transformDirection(directions.get(0)),fields.get(0));
        for (int i = 1; i < fields.size(); i++)
            newSort = newSort.and(Sort.by(transformDirection(directions.get(i)), fields.get(i)));
        return newSort;
    }

    @Override
    public String toString() {
        return "SortDto{" +
                "fields=" + fields +
                ", directions=" + directions +
                '}';
    }
}
