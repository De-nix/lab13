import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientComponent} from "./client/client.component";
import {ClientDetailComponent} from "./client/client-detail/client-detail.component";
import {ClientNewComponent} from "./client/client-new/client-new.component";
import {BookComponent} from "./book/book.component";
import {BookDetailComponent} from "./book/book-detail/book-detail.component";
import {BookAddComponent} from "./book/book-add/book-add.component";
import {BookListComponent} from "./book/book-list/book-list.component";
import {ClientListComponent} from "./client/client-list/client-list.component";
import {ClientBuyComponent} from "./client/client-buy/client-buy.component";


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'client', component: ClientComponent},
  {path: 'client/buy/:id', component: ClientBuyComponent},
  {path: 'client/list', component: ClientListComponent},
  {path: 'client/detail/:id', component: ClientDetailComponent},
  {path: 'client/add', component: ClientNewComponent},

  {path: 'book', component: BookComponent},
  {path: 'book/list', component: BookListComponent},
  {path: 'book/detail/:id', component: BookDetailComponent},
  {path: 'book/add', component: BookAddComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
