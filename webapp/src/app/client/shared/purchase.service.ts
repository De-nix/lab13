import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Purchase} from "./purchase.model";


@Injectable()
export class PurchaseService {
  private purchasesUrl = 'http://localhost:8080/api/purchases';

  constructor(private httpClient: HttpClient) {
  }

  getBooks(bid: number): Observable<Purchase[]> {

    const url = `${this.purchasesUrl}/${bid}`;
    return this.httpClient.get<Array<Purchase>>(url);
  }

  update(id: number, purchase: Purchase): any {
    const url = this.purchasesUrl + "/" + id;
    this.httpClient.put<void>(url, {
      "idClient": purchase.idClient,
      "idBook": purchase.idBook,
      "bookName": purchase.bookName,
      "date": purchase.date
    }).subscribe();

  }


  getClients(id: number) {
    const url = `${this.purchasesUrl}/book/${id}`;
    return this.httpClient.get<Array<Purchase>>(url);
  }
}
