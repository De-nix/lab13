import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Client} from "./client.model";

import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable()
export class ClientService {
  private clientsUrl = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsUrl);
  }

  getClient(id: number): Observable<Client> {
    return this.getClients()
      .pipe(
        map(clients => clients.find(client => client.id === id))
      );
  }
  saveClient(name: string): Observable<Client> {
    const client = {name};
    console.log("saveClient", client);

    return this.httpClient
      .post<Client>(this.clientsUrl, client);
  }

  update(client): Observable<Client> {
    const url = `${this.clientsUrl}/${client.id}`;
    return this.httpClient
      .put<Client>(url, client);
  }

  deleteClient(id: number): Observable<Client> {
    const url = `${this.clientsUrl}/${id}`;
    return this.httpClient
      .delete<Client>(url);
  }


  getClientsWithBook(value: string) {
    const url = this.clientsUrl + "/book/" + value;
    return this.httpClient.get<Array<Client>>(url);
  }
  getClientsWithPurchase(value: string) {
    const url = this.clientsUrl + "/purchase/" + value;
    return this.httpClient.get<Array<Client>>(url);
  }
}
