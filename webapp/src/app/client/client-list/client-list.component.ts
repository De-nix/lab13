import {Component, OnInit} from '@angular/core';
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'app-client',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit {
  errorMessage: string;
  clients: Client[];
  selectedClient: Client;
  noClients: number;

  constructor(private clientService: ClientService,
              private router: Router, private location: Location) {
  }

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.clientService.getClients()
      .subscribe(
        clients => this.clients = clients
      );
  }

  onSelect(client: Client): void {
    this.selectedClient = client;
  }


  gotoDetail(): void {
    this.router.navigate(['/client/detail', this.selectedClient.id]);
  }

  deleteClient(client: Client) {
    console.log("deleting client: ", client);

    this.clientService.deleteClient(client.id)
      .subscribe(_ => {
        console.log("client deleted");

        this.clients = this.clients.filter(s => s !== client);
        if (this.selectedClient === client) {
          this.selectedClient = null;
        }
      });
  }


  goBack(): void {
    this.location.back();
  }


  getClientsWithPurchase(value: string) {
    this.clientService.getClientsWithPurchase(value).subscribe(
      clients => this.clients = clients
    );
  }
  getClientsWithBook( value: string) {
    this.clientService.getClientsWithBook(value).subscribe(
      clients => this.clients = clients
    );
  }
}
