import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../../book/shared/book.model";
import {Client} from "../shared/client.model";
import {Purchase} from "../shared/purchase.model";
import {ClientService} from "../shared/client.service";
import {PurchaseService} from "../shared/purchase.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Location} from "@angular/common";
import {switchMap} from "rxjs/operators";
import {BookService} from "../../book/shared/book.service";

@Component({
  selector: 'app-client-buy',
  templateUrl: './client-buy.component.html',
  styleUrls: ['./client-buy.component.css']
})
export class ClientBuyComponent implements OnInit {
  books: Book[];
  @Input() client: Client;
  selectedBook: Book;
  constructor(private clientService: ClientService,
              private bookService: BookService,
              private route: ActivatedRoute,
              private location: Location
  ) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.clientService.getClient(+params['id'])))
      .subscribe(client => this.client = client);
    this.bookService.getBooks().subscribe(
      books => this.books = books );
  }

  buy(book: Book): void {
    this.client.books.push(book.id);
  }
  goBack(): void {
    this.location.back();
  }

  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  save(): void {
    this.clientService.update(this.client)
      .subscribe(_ => this.goBack());
  }
}
