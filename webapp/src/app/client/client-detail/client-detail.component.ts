import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Location} from '@angular/common';

import {switchMap} from "rxjs/operators";
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";
import {Purchase} from "../shared/purchase.model";
import {PurchaseService} from "../shared/purchase.service";


@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css'],
})

export class ClientDetailComponent implements OnInit {

  @Input() client: Client;
  purchases: Purchase[];
  selectedPurchase: Purchase;
  constructor(private clientService: ClientService,
              private purchaseService: PurchaseService,
              private route: ActivatedRoute,
              private location: Location,
              private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getClient();
  }

  getClient(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.clientService.getClient(+params['id'])))
      .subscribe(client => {
        this.client = client;
        this.purchaseService.getBooks(
          this.client.id
        ).subscribe(purchases => {
          const books = [];
          this.purchases = purchases;
          purchases.forEach(function (purchase) {
            books.push(purchase.idBook);
          } );
          this.client.books = books;
        });
      });
  }

  buy(): void {
      this.router.navigate(['/client/buy', this.client.id]);
  }
  goBack(): void {
    this.location.back();
  }

  onSelect(purchase: Purchase): void {
    this.selectedPurchase = purchase;
  }

  deletePurchase(purchase: Purchase): void {
    this.client.books = this.client.books.filter(function(value) { return value !== purchase.idBook; });
    this.save();
  }
  gotoDetail(): void {
   // this.fillBooks();
    this.router.navigate(['/book/detail', this.selectedPurchase.idBook]);
  }
  save(): void {
    this.clientService.update(this.client).subscribe();
  }
  dateChanged(): void {
    this.purchaseService.update(this.client.id, this.selectedPurchase);
    this.getClient();
  }
}
