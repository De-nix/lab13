import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {BookService} from "../shared/book.service";
import {Book} from "../shared/book.model";

import {Location} from '@angular/common';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  errorMessage: string;
  books: Book[];
  selectedBook: Book;
  noBooks: number;
  constructor(private bookService: BookService,
              private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe(
      books => this.books = books
    );
  }



  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  gotoDetail(): void {
    this.router.navigate(['/book/detail', this.selectedBook.id]);
  }

  deleteBook(book: Book) {
    console.log("deleting book: ", book);

    this.bookService.deleteBook(book.id)
      .subscribe(_ => {
        console.log("book deleted");

        this.books = this.books
          .filter(s => s.id !== book.id);
      });
  }


  goBack(): void {
    this.location.back();
  }
}
