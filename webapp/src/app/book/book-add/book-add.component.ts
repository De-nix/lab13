import { Component, OnInit } from '@angular/core';
import {BookService} from "../shared/book.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  constructor(private bookService: BookService, private location: Location) { }

  ngOnInit(): void {
  }

  saveBook(title: string, author: string, price: string) {
    console.log("saving book", title, author, price);

    this.bookService.saveBook({
      id: 0,
      title,
      author,
      price: +price
    })
      .subscribe(book => console.log("saved book: ", book));

    this.location.back(); // ...
  }

  goBack(): void {
    this.location.back();
  }
}
