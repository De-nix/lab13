import { Component, Input, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import {switchMap} from "rxjs/operators";
import {BookService} from "../shared/book.service";
import {Book} from "../shared/book.model";
import {Purchase} from "../../client/shared/purchase.model";
import {PurchaseService} from "../../client/shared/purchase.service";


@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  @Input() book: Book;

  purchases: Purchase[];
  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private purchaseService: PurchaseService,
              private location: Location) { }


  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.bookService.getBook(+params['id'])))
      .subscribe(book => {
        this.book = book;
        this.purchaseService.getClients(
          this.book.id
        ).subscribe(purchases => {
          this.purchases = purchases;
        });
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.bookService.update(this.book)
      .subscribe(_ => this.goBack());
  }
}
