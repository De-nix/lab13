import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ClientDetailComponent} from "./client/client-detail/client-detail.component";
import {ClientComponent} from "./client/client.component";
import {ClientListComponent} from "./client/client-list/client-list.component";
import {ClientService} from "./client/shared/client.service";
import { ClientNewComponent } from './client/client-new/client-new.component';
import { BookComponent } from './book/book.component';
import { BookAddComponent } from './book/book-add/book-add.component';
import { BookListComponent } from './book/book-list/book-list.component';
import { BookDetailComponent } from './book/book-detail/book-detail.component';
import {BookService} from "./book/shared/book.service";
import {PurchaseService} from "./client/shared/purchase.service";
import { ClientBuyComponent } from './client/client-buy/client-buy.component';


@NgModule({
  declarations: [
    AppComponent,
    ClientDetailComponent,
    ClientComponent,
    ClientListComponent,
    ClientNewComponent,
    BookComponent,
    BookAddComponent,
    BookListComponent,
    BookDetailComponent,
    ClientBuyComponent,



  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [BookService, ClientService, PurchaseService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
